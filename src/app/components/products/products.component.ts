import { Component, OnInit } from '@angular/core';
import {
  Modal,
  Ripple,
  initTE,
} from "tw-elements";
import { initFlowbite } from 'flowbite';


@Component({
  selector: 'app-products',
  templateUrl: './products.component.html',
  styleUrls: ['./products.component.scss']
})
export class ProductsComponent  implements OnInit {

  showModalBox: boolean = false;


  onSave(event?: MouseEvent) {
    if(0){
      // Dont open the modal
      this.showModalBox = false;
    } else {
       // Open the modal
       this.showModalBox = true;
    }
  }

  public  ngOnInit(): void{
    initFlowbite();
    initTE({ Modal, Ripple });
    
  }
}
