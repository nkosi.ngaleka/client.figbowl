import { NgModule } from '@angular/core';
import { Carousel, Dropdown, initTE } from 'tw-elements';
import { RouterModule, Routes } from '@angular/router';
import { LoginComponent } from './components/login/login.component';
import { DashboardComponent } from './components/dashboard/dashboard.component';
import { InboxComponent } from './components/inbox/inbox.component';
import { OrdersComponent } from './components/orders/orders.component';
import { ProductsComponent } from './components/products/products.component';
import { MenusComponent } from './components/menus/menus.component';

const routes: Routes = [
  {path:"", redirectTo:"login", pathMatch:"full"},
  {path:"login", component:LoginComponent},
  {path:"dashboard", component:DashboardComponent},
  {path:"inbox", component:InboxComponent},
  {path:"orders", component:OrdersComponent},
  {path:"products", component:ProductsComponent},
  {path:"menus", component:MenusComponent},
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { 
  ngOnInit() {
    initTE({ Carousel, Dropdown });
  }
}
